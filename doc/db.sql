CREATE USER gibddonline_decrees_push_service WITH PASSWORD 'gibddonline_decrees_push_service';
GRANT select, update, delete ON imoney_app_search_terms TO gibddonline_decrees_push_service;

DROP VIEW decrees_for_today;
CREATE OR REPLACE VIEW decrees_for_today AS
 SELECT phone_token, phone_os, total_count, total_amount
	FROM (
	 SELECT t.phone_token,
		CASE
		    WHEN char_length(t.phone_token::text) = 64 THEN 1
		    WHEN char_length(t.phone_token::text) = 162 THEN 2
		    ELSE 0
		END AS phone_os,
	    count(t.phone_token) AS total_count,
	    sum(COALESCE(t.amount, 0.00)) AS total_amount
	   FROM ( SELECT DISTINCT st.phone_token, d.id_decree, d.amount
		   FROM ( SELECT imoney_app_search_terms.id_search_term,
			    imoney_app_search_terms.phone_token,
			    imoney_app_search_terms.driver_license_number,
			    imoney_app_search_terms.vehicle_registration_number,
			    imoney_app_search_terms.registration_certificate_number,
			    imoney_app_search_terms.found_decrees_count,
			    imoney_app_search_terms.creation_dt,
			    imoney_app_search_terms.last_updating_dt,
			    imoney_app_search_terms.scanned
			   FROM imoney_app_search_terms
			  WHERE imoney_app_search_terms.phone_token IS NOT NULL AND (char_length(imoney_app_search_terms.phone_token::text) = 64 OR char_length(imoney_app_search_terms.phone_token::text) = 162)) st,
		    ( SELECT decrees.id_decree, decrees.amount, decrees.driver_license_number,
			    decrees.vehicle_registration_number,
			    decrees.registration_certificate_number
			   FROM decrees
			  WHERE decrees.id_decree_status = 1 AND decrees.number IS NOT NULL AND decrees.dt IS NOT NULL AND date_trunc('day'::text, decrees.creation_dt) = date_trunc('day'::text, now())) d
		  WHERE st.driver_license_number IS NOT NULL AND upper(d.driver_license_number::text) = upper(st.driver_license_number::text) OR st.driver_license_number IS NULL AND upper(d.vehicle_registration_number::text) = upper(st.vehicle_registration_number::text) AND upper(d.registration_certificate_number::text) = upper(st.registration_certificate_number::text)) t
	  GROUP BY t.phone_token
	) AS t;
	/*
	UNION ALL
	(SELECT '5183617f8e4e55bf7ee7cd69575daff197b21af3d906218edc98783dab0eb934', 1, 1, -1)
	UNION ALL
	(SELECT '3e396ee67c94df8545f737216201cee96d2084807a8f376d8cee1377279fa31b', 1, 1, -1)-- Паша;
	*/

GRANT select ON decrees_for_today TO gibddonline_decrees_push_service;