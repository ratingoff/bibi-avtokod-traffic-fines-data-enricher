package ru.ratingoff.bibi.avtokod.enricher;

import org.apache.camel.Consume;
import org.apache.camel.language.XPath;
import org.apache.commons.codec.binary.Base64;
import org.jclouds.ContextBuilder;
import org.jclouds.aws.s3.AWSS3AsyncClient;
import org.jclouds.aws.s3.AWSS3Client;
import org.jclouds.aws.s3.blobstore.AWSS3BlobStoreContext;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.http.HttpResponseException;
import org.jclouds.rest.RestContext;
import org.jclouds.s3.domain.CannedAccessPolicy;
import org.jclouds.s3.domain.S3Object;
import org.jclouds.s3.options.PutObjectOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ratingoff.bibi.avtokod.scraper.AvtokodScraperService;
import ru.ratingoff.bibi.avtokod.scraper.AvtokodTrafficFine;
import ru.ratingoff.bibi.avtokod.scraper.ScraperException;
import ru.ratingoff.bibi.common.dao.CarDAO;
import ru.ratingoff.bibi.common.dao.TrafficFineDAO;
import ru.ratingoff.bibi.common.model.AdditionalDataLoadStatus;
import ru.ratingoff.bibi.common.model.Car;
import ru.ratingoff.bibi.common.model.TrafficFine;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class AvtokodTrafficFinesDataEnricherServiceImpl implements AvtokodTrafficFinesDataEnricherService {

    private static Logger logger = LoggerFactory.getLogger(AvtokodTrafficFinesDataEnricherServiceImpl.class);

    private static final AdditionalDataLoadStatus ADDITIONAL_DATA_LOAD_STATUS$WAS_NOT_LOADED = new AdditionalDataLoadStatus(1, null);
    private static final AdditionalDataLoadStatus ADDITIONAL_DATA_LOAD_STATUS$LOADING = new AdditionalDataLoadStatus(2, null);
    private static final AdditionalDataLoadStatus ADDITIONAL_DATA_LOAD_STATUS$LOADED = new AdditionalDataLoadStatus(3, null);
    private static final AdditionalDataLoadStatus ADDITIONAL_DATA_LOAD_STATUS$ABSENT = new AdditionalDataLoadStatus(4, null);

    private class TrafficFineEntry {
        private TrafficFine trafficFine;
        private boolean used;

        private TrafficFineEntry(TrafficFine trafficFine) {
            this.trafficFine = trafficFine;
        }

        public TrafficFine getTrafficFine() {
            return trafficFine;
        }

        public void setTrafficFine(TrafficFine trafficFine) {
            this.trafficFine = trafficFine;
        }

        public boolean isUsed() {
            return used;
        }

        public void setUsed(boolean used) {
            this.used = used;
        }
    }

    private TrafficFineDAO trafficFineDAO;
    private CarDAO carDAO;
    private AvtokodScraperService avtokodScraperService;
    private String awsAccessKey;
    private String awsSecretKey;
    private RestContext<AWSS3Client, AWSS3AsyncClient> s3providerContext;

    public TrafficFineDAO getTrafficFineDAO() {
        return trafficFineDAO;
    }

    public void setTrafficFineDAO(TrafficFineDAO trafficFineDAO) {
        this.trafficFineDAO = trafficFineDAO;
    }

    public CarDAO getCarDAO() {
        return carDAO;
    }

    public void setCarDAO(CarDAO carDAO) {
        this.carDAO = carDAO;
    }

    public AvtokodScraperService getAvtokodScraperService() {
        return avtokodScraperService;
    }

    public void setAvtokodScraperService(AvtokodScraperService avtokodScraperService) {
        this.avtokodScraperService = avtokodScraperService;
    }

    public String getAwsAccessKey() {
        return awsAccessKey;
    }

    public void setAwsAccessKey(String awsAccessKey) {
        this.awsAccessKey = awsAccessKey;
    }

    public String getAwsSecretKey() {
        return awsSecretKey;
    }

    public void setAwsSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }

    public void init() throws Exception {
        if (s3providerContext == null) {
            synchronized (this) {
                if (s3providerContext == null) {
                    BlobStoreContext s3context = ContextBuilder.newBuilder("aws-s3")
                            .credentials(awsAccessKey, awsSecretKey)
                            .buildView(AWSS3BlobStoreContext.class);
                    s3providerContext = s3context.unwrap();
                    logger.info("Amazon S3 context provider initiated");
                }
            }
        }
    }

    public void destroy() throws Exception {
        if(s3providerContext != null) {
            synchronized (this) {
                if(s3providerContext != null) {
                    s3providerContext.close();
                    s3providerContext = null;
                    logger.info("Amazon S3 context provider destroyed");
                }
            }
        }
    }

    // <driverLicenseNumber>7701445893</driverLicenseNumber>
    // <driverLicenseNumber>77ВА181059</driverLicenseNumber>
    @Consume(uri="jms:avtokodTrafficFinesDataEnricher?jmsMessageType=Text&disableReplyTo=true")
    @Override
    public void enrichData(
            @XPath("/args/driverLicenseNumber/text()")
            String driverLicenseNumber,
            @XPath("/args/driverLicenseIssued/text()")
            String driverLicenseIssuedAsString,
            @XPath("/args/vehicleRegistrationCertificateNumber/text()")
            String vehicleRegistrationCertificateNumber,
            @XPath("/args/vehicleRegistrationNumber/text()")
            String vehicleRegistrationNumber,
            @XPath("/args/carId/text()")
            String carId
    ) {
        if(logger.isDebugEnabled()) {
            logger.debug("started the car data enriching: cardId={}", carId);
        }
        Date driverLicenseIssued = null;
        if(!isNullOrEmpty(driverLicenseIssuedAsString)) {
            try {
                driverLicenseIssued = new SimpleDateFormat("yyyy-MM-dd").parse(driverLicenseIssuedAsString);
            } catch (ParseException e) {
                throw new IllegalArgumentException("driverLicenseIssued has invalid date format: " + e.getMessage());
            }
        }

        if((isNullOrEmpty(driverLicenseNumber) && driverLicenseIssued == null)
                && (isNullOrEmpty(vehicleRegistrationCertificateNumber) && isNullOrEmpty(vehicleRegistrationNumber))
                && isNullOrEmpty(carId)) {
            throw new IllegalArgumentException("(driverLicenseNumber and driverLicenseIssued) or (vehicleRegistrationCertificateNumber and vehicleRegistrationNumber) or carId must be not null and not empty");
        }

        Car car = null;
        if (!isNullOrEmpty(carId)) {
            car = carDAO.findCarById(carId);
            if (car == null) {
                throw new IllegalArgumentException("carId[" + carId + "] is incorrect");
            }
            driverLicenseNumber = car.getDriverLicenseNumber();
            driverLicenseIssued = car.getDriverLicenseIssued();
            vehicleRegistrationNumber = car.getRegistrationNumber();
            vehicleRegistrationCertificateNumber = car.getRegistrationCertificateNumber();
        }

        List<AvtokodTrafficFine> trafficFinesFromAvtokod = getTrafficFinesFromAvtokod(
                driverLicenseNumber,
                driverLicenseIssued,
                vehicleRegistrationNumber,
                vehicleRegistrationCertificateNumber
        );
        if(logger.isDebugEnabled()) {
            logger.debug("amount of the trafficFines from avtokod.mos.ru: {}", trafficFinesFromAvtokod.size());
        }

        List<TrafficFine> trafficFinesFromDatabase = getTrafficFinesFromDatabase(
                driverLicenseNumber,
                vehicleRegistrationNumber,
                vehicleRegistrationCertificateNumber,
                carId
        );
        if(logger.isDebugEnabled()) {
            logger.debug("amount of the trafficFines from the database: {}", trafficFinesFromDatabase.size());
        }

        Map<String, TrafficFineEntry> existingTrafficFines = new HashMap<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        for(TrafficFine trafficFine : trafficFinesFromDatabase) {
            if(logger.isDebugEnabled()) {
                logger.debug("trafficFine: id={}, number={}, date={}, additionalDataLoadStatus={}", trafficFine.getId(), trafficFine.getNumber(), trafficFine.getDate(), trafficFine.getAdditionalDataLoadStatus().getId());
            }
            if(!trafficFine.getNumber().replace(" ", "").isEmpty() && trafficFine.getBill() != null) {
                existingTrafficFines.put(
                        trafficFine.getNumber().toUpperCase() + "|" + simpleDateFormat.format(trafficFine.getDate()),
                        new TrafficFineEntry(trafficFine)
                );
            } else {
                changeAdditionalDataLoadStatusToABSENT(trafficFine);
            }
        }

        if(logger.isDebugEnabled()) {
            logger.debug("started working with trafficFinesFromAvtokod");
        }
        for(AvtokodTrafficFine avtokodTrafficFine : trafficFinesFromAvtokod) {
            TrafficFineEntry trafficFineEntry = existingTrafficFines.get(avtokodTrafficFine.getNumber().toUpperCase() + "|" + simpleDateFormat.format(avtokodTrafficFine.getDate()));
            if (trafficFineEntry == null) {
                continue;
            }
            trafficFineEntry.setUsed(true);

            TrafficFine trafficFine = trafficFineEntry.getTrafficFine();
            trafficFine.setTitle(avtokodTrafficFine.getTitle());
            trafficFine.setViolation(avtokodTrafficFine.getViolation());
            trafficFine.setViolationDatetime(avtokodTrafficFine.getViolationDatetime());
            trafficFine.setViolationScene(avtokodTrafficFine.getViolationScene());
            trafficFine.setGovernmentAgencyIdentifiedViolation(avtokodTrafficFine.getGovernmentAgencyIdentifiedViolation());
            trafficFine.setViolator(avtokodTrafficFine.getViolator());
            trafficFine.setVehicleRegistrationNumber(avtokodTrafficFine.getVehicleRegistrationNumber());
            trafficFine.setCreationPlace(avtokodTrafficFine.getCreationPlace());
            trafficFine.setAdditionalDataLoadStatus(ADDITIONAL_DATA_LOAD_STATUS$LOADED);
            trafficFine.setLastAdditionalDataLoad(new Date());

            if (trafficFine.getViolationPhotos() == null || trafficFine.getViolationPhotos().isEmpty()) {
                List<String> violationPhotosLinks = new ArrayList<>(2);
                try {
                    String[] violationPhotos = avtokodScraperService.getViolationPhotos(avtokodTrafficFine.getViolationPhotosReferenceId());
                    for(int i = 0; i < violationPhotos.length; i++) {
                        if(i >= 0 && i < 2) {
                            byte[] violationPhotoAsBytes = Base64.decodeBase64(violationPhotos[i]);
                            String violationPhotoId = saveViolationPhotoToS3(violationPhotoAsBytes);
                            violationPhotosLinks.add(violationPhotoId);
                        }
                    }
                } catch (ScraperException e) {
                    logger.error(
                        "Can't get the traffic fine photos from avtokod.mos.ru [violationPhotosReferenceId={}]: {}",
                        avtokodTrafficFine.getViolationPhotosReferenceId(),
                        e.getMessage()
                    );
                    if(logger.isDebugEnabled()) {
                        logger.debug(
                            "Can't get the traffic fine photos from avtokod.mos.ru [violationPhotosReferenceId={}]: {}",
                            avtokodTrafficFine.getViolationPhotosReferenceId(),
                            e.getMessage(),
                            e
                        );
                    }
                } catch (HttpResponseException e) {
                    logger.error("Can't save the traffic fine photos in Amazon S3: {}", e.getMessage());
                    if(logger.isDebugEnabled()) {
                        logger.debug("Can't save the traffic fine photos in Amazon S3: {}", e.getMessage(), e);
                    }
                }
                trafficFine.setViolationPhotos(violationPhotosLinks);
            }
            if(logger.isDebugEnabled()) {
                logger.debug("data loaded for trafficFine: id={}, number={}, date={}, additionalDataLoadStatus={}]", trafficFine.getId(), trafficFine.getNumber(), trafficFine.getDate(), trafficFine.getAdditionalDataLoadStatus().getId());
            }
            trafficFineDAO.merge(trafficFine);
        }
        if(logger.isDebugEnabled()) {
            logger.debug("ended working with trafficFinesFromAvtokod");
            logger.debug("existingTrafficFines.size() = {}", existingTrafficFines.values().size());
        }
        for(TrafficFineEntry trafficFineEntry : existingTrafficFines.values()) {
            if(!trafficFineEntry.isUsed()) {
                TrafficFine trafficFine = trafficFineEntry.getTrafficFine();
                if(logger.isDebugEnabled()) {
                    logger.debug("data not loaded for trafficFine: id={}, number={}, date={}, additionalDataLoadStatus={}", trafficFine.getId(), trafficFine.getNumber(), trafficFine.getDate(), trafficFine.getAdditionalDataLoadStatus().getId());
                }
                changeAdditionalDataLoadStatusToABSENT(trafficFine);
            }
        }

        if (car != null) {
            car = carDAO.findCarById(carId);
            car.setLastAdditionalDataLoad(new Date());
            carDAO.saveOrUpdate(car);
        }
        if(logger.isDebugEnabled()) {
            logger.debug("ended the car data enriching: cardId={}", carId);
        }
    }

    private List<TrafficFine> getTrafficFinesFromDatabase(String driverLicenseNumber, String vehicleRegistrationNumber, String vehicleRegistrationCertificateNumber, String carId) {
        if (!isNullOrEmpty(carId)) {
            return trafficFineDAO.findTrafficFinesByCarId(carId);
        } else if (!isNullOrEmpty(driverLicenseNumber)) {
            return trafficFineDAO.findTrafficFinesByDriver(driverLicenseNumber);
        } else if (!isNullOrEmpty(vehicleRegistrationCertificateNumber) && !isNullOrEmpty(vehicleRegistrationNumber)) {
            return trafficFineDAO.findTrafficFinesByVehicle(vehicleRegistrationCertificateNumber, vehicleRegistrationNumber);
        }
        return Collections.emptyList();
    }

    private List<AvtokodTrafficFine> getTrafficFinesFromAvtokod(String driverLicenseNumber, Date driverLicenseIssued, String vehicleRegistrationNumber, String vehicleRegistrationCertificateNumber) {
        List<AvtokodTrafficFine> result = new ArrayList<>(0);
        if(!isNullOrEmpty(driverLicenseNumber) && driverLicenseIssued != null) {
            List<AvtokodTrafficFine> trafficFines = callGetTrafficFinesFromAvtokod(
                driverLicenseNumber,
                driverLicenseIssued,
                null,
                null
            );
            result.addAll(trafficFines);
        }
        if (!isNullOrEmpty(vehicleRegistrationCertificateNumber) && !isNullOrEmpty(vehicleRegistrationNumber)) {
            List<AvtokodTrafficFine> trafficFines = callGetTrafficFinesFromAvtokod(
                null,
                null,
                vehicleRegistrationNumber,
                vehicleRegistrationCertificateNumber
            );
            result.addAll(trafficFines);
        }
        return result;
    }

    private List<AvtokodTrafficFine> callGetTrafficFinesFromAvtokod(String driverLicenseNumber, Date driverLicenseIssued, String vehicleRegistrationNumber, String vehicleRegistrationCertificateNumber) {
        try {
            return avtokodScraperService.getTrafficFines(driverLicenseNumber, driverLicenseIssued, vehicleRegistrationNumber, vehicleRegistrationCertificateNumber);
        } catch (ScraperException e) {
            logger.error(
                "Can't get the traffic fines from avtokod.mos.ru [driverLicenseNumber={}, driverLicenseIssued={}, vehicleRegistrationCertificateNumber={}, vehicleRegistrationNumber={}]: {}",
                driverLicenseNumber,
                driverLicenseIssued,
                vehicleRegistrationCertificateNumber,
                vehicleRegistrationNumber,
                e.getMessage()
            );
            if(logger.isDebugEnabled()) {
                logger.debug(
                    "Can't get the traffic fines from avtokod.mos.ru [driverLicenseNumber={}, vehicleRegistrationCertificateNumber={}, vehicleRegistrationNumber={}]: {}",
                    driverLicenseNumber,
                    driverLicenseIssued,
                    vehicleRegistrationCertificateNumber,
                    vehicleRegistrationNumber,
                    e.getMessage(),
                    e
                );
            }
            return Collections.emptyList();
        }
    }

    private String saveViolationPhotoToS3(byte[] violationPhotoAsBytes) throws HttpResponseException {
        String key = UUID.randomUUID().toString().replace("-", "");
        AWSS3Client s3client = s3providerContext.getApi();
        S3Object object = s3client.newS3Object();
        object.setPayload(violationPhotoAsBytes);
        object.getMetadata().setCacheControl("max-age=3600");
        object.getMetadata().getContentMetadata().setContentType("image/png");
        object.getMetadata().setKey("trafficFines/violationPhotos/" + key);
        s3client.putObject("bibiservice", object, new PutObjectOptions().withAcl(CannedAccessPolicy.PUBLIC_READ));
        return key;
    }

    private void changeAdditionalDataLoadStatusToABSENT(TrafficFine trafficFine) {
        if(trafficFine.getAdditionalDataLoadStatus().getId() != ADDITIONAL_DATA_LOAD_STATUS$LOADED.getId()) {
            trafficFine.setAdditionalDataLoadStatus(ADDITIONAL_DATA_LOAD_STATUS$ABSENT);
        }
        trafficFine.setLastAdditionalDataLoad(new Date());
        trafficFineDAO.merge(trafficFine);
    }

    private boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

    private boolean isDriverLicenseDataCorrect(Car car) {
        return isDriverLicenseDataCorrect(car.getDriverLicenseNumber(), car.getDriverLicenseIssued());
    }

    private boolean isDriverLicenseDataCorrect(String driverLicenseNumber, Date driverLicenseIssued) {
        return !isNullOrEmpty(driverLicenseNumber) && driverLicenseIssued != null;
    }

    private boolean isVehicleRegistrationDataCorrect(Car car) {
        return isVehicleRegistrationDataCorrect(car.getRegistrationCertificateNumber(), car.getRegistrationNumber());
    }

    private boolean isVehicleRegistrationDataCorrect(String registrationCertificateNumber, String registrationNumber) {
        return !isNullOrEmpty(registrationCertificateNumber) && !isNullOrEmpty(registrationNumber);
    }



}
