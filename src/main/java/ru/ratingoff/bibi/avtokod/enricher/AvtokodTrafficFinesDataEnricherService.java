package ru.ratingoff.bibi.avtokod.enricher;

import java.util.Date;

public interface AvtokodTrafficFinesDataEnricherService {
    void enrichData(String driverLicenseNumber, String driverLicenseIssued, String vehicleRegistrationCertificateNumber, String vehicleRegistrationNumber, String carId);
}
