package ru.ratingoff.bibi.avtokod.enricher;

public class DataEnricherException extends Exception {
    public DataEnricherException() {
    }

    public DataEnricherException(String message) {
        super(message);
    }

    public DataEnricherException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataEnricherException(Throwable cause) {
        super(cause);
    }

    public DataEnricherException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
