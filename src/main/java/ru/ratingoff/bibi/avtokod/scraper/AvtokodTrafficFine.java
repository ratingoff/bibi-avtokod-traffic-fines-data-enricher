package ru.ratingoff.bibi.avtokod.scraper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AvtokodTrafficFine {
    // наименование документа
    private String title;
    // номер постановления
    private String number;
    // дата постановления
    private Date date;
    // сумма штрафа
    private String amount;
    // статус оплаты штрафа: оплачен / не оплачен
    private boolean paid;
    // статья нарушения
    private String violation;
    // фотографии с места нарушения
    private String violationPhotosReferenceId;
    // дата и время нарушения
    private Date violationDatetime;
    // место нарушения
    private String violationScene;
    // орган власти, выявивший нарушение
    private String governmentAgencyIdentifiedViolation;
    // нарушитель
    private String violator;
    // транспортное средство
    private String vehicleRegistrationNumber;
    // место составления документа
    private String creationPlace;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public String getViolation() {
        return violation;
    }

    public void setViolation(String violation) {
        this.violation = violation;
    }

    public String getViolationPhotosReferenceId() {
        return violationPhotosReferenceId;
    }

    public void setViolationPhotosReferenceId(String violationPhotosReferenceId) {
        this.violationPhotosReferenceId = violationPhotosReferenceId;
    }

    public Date getViolationDatetime() {
        return violationDatetime;
    }

    public void setViolationDatetime(Date violationDatetime) {
        this.violationDatetime = violationDatetime;
    }

    public String getViolationScene() {
        return violationScene;
    }

    public void setViolationScene(String violationScene) {
        this.violationScene = violationScene;
    }

    public String getGovernmentAgencyIdentifiedViolation() {
        return governmentAgencyIdentifiedViolation;
    }

    public void setGovernmentAgencyIdentifiedViolation(String governmentAgencyIdentifiedViolation) {
        this.governmentAgencyIdentifiedViolation = governmentAgencyIdentifiedViolation;
    }

    public String getViolator() {
        return violator;
    }

    public void setViolator(String violator) {
        this.violator = violator;
    }

    public String getVehicleRegistrationNumber() {
        return vehicleRegistrationNumber;
    }

    public void setVehicleRegistrationNumber(String vehicleRegistrationNumber) {
        this.vehicleRegistrationNumber = vehicleRegistrationNumber;
    }

    public String getCreationPlace() {
        return creationPlace;
    }

    public void setCreationPlace(String creationPlace) {
        this.creationPlace = creationPlace;
    }
}
