package ru.ratingoff.bibi.avtokod.scraper;

import java.util.Date;
import java.util.List;

public interface AvtokodScraperService {
    List<AvtokodTrafficFine> getTrafficFines(String driverLicenseNumber, Date driverLicenseIssued, String vehicleRegistrationNumber, String vehicleLicenseNumber) throws ScraperException;
    String[] getViolationPhotos(String violationPhotosReferenceId) throws ScraperException;
}
