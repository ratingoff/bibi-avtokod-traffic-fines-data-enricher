package ru.ratingoff.bibi.avtokod.scraper;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AvtokodScraperServiceImpl implements AvtokodScraperService {
    private String serviceUrl;
    private String login;
    private String password;
    private CloseableHttpClient httpClient;
    private int socketTimeout = 600000;
    private int connectTimeout = 5000;
    private int connectionPoolSize = 5;
    private ObjectMapper objectMapper;

    public AvtokodScraperServiceImpl(String serviceUrl, String login, String password, int connectionPoolSize, int connectTimeout, int socketTimeout) {
        this.serviceUrl = serviceUrl;
        this.login = login;
        this.password = password;
        this.connectionPoolSize = connectionPoolSize;
        this.connectTimeout = connectTimeout;
        this.socketTimeout = socketTimeout;
        this.httpClient = HttpClients.custom()
                .setMaxConnPerRoute(connectionPoolSize)
                .setMaxConnTotal(connectionPoolSize)
                .setDefaultRequestConfig(
                        RequestConfig.custom()
                                .setSocketTimeout(socketTimeout)
                                .setConnectTimeout(connectTimeout)
                                .build()
                )
                .build();
        this.objectMapper = new ObjectMapper();
        try {
            auth();
        } catch (ScraperException e) {
        }
    }

    public void destroy() throws Exception {
        httpClient.close();
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public String getLogin() {
        return login;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public int getConnectionPoolSize() {
        return connectionPoolSize;
    }

    @Override
    public List<AvtokodTrafficFine> getTrafficFines(String driverLicenseNumber, Date driverLicenseIssued, String vehicleRegistrationNumber, String vehicleLicenseNumber) throws ScraperException {
        try {
            HttpPost httpPost = new HttpPost(serviceUrl + "/trafficFines");
            StringBuilder json = new StringBuilder("{");
            if(!isEmpty(driverLicenseNumber)) {
                json.append("\"driver_license_number\": \"").append(driverLicenseNumber).append("\"");
                json.append("\"driver_license_issued\": \"").append(new SimpleDateFormat("yyyy-MM-dd").format(driverLicenseIssued)).append("\"");
            } else {
                json.append("\"vehicle_registration_number\": \"").append(vehicleRegistrationNumber).append("\",");
                json.append("\"vehicle_license_number\": \"").append(vehicleLicenseNumber).append("\"");
            }
            json.append("}");
            httpPost.setEntity(
                    EntityBuilder.create()
                            .setContentEncoding("UTF-8")
                            .setContentType(ContentType.APPLICATION_JSON)
                            .setText(json.toString())
                            .build()
            );
            CloseableHttpResponse response = httpClient.execute(httpPost);
            try {
                if (response.getStatusLine().getStatusCode() != 200) {
                    if (response.getStatusLine().getStatusCode() == 401) {
                        auth();
                        return getTrafficFines(driverLicenseNumber, driverLicenseIssued, vehicleRegistrationNumber, vehicleLicenseNumber);
                    }
                    String errorMessage = EntityUtils.toString(response.getEntity(), "UTF-8");
                    throw new ScraperException(errorMessage);
                }
                List<AvtokodTrafficFine> trafficFines = objectMapper.readValue(
                        response.getEntity().getContent(),
                        TypeFactory.defaultInstance()
                                .constructParametricType(
                                        List.class, AvtokodTrafficFine.class
                                )
                );
                EntityUtils.consumeQuietly(response.getEntity());
                return trafficFines;
            } finally {
                response.close();
            }
        } catch (IOException e) {
            throw new ScraperException(e.getMessage(), e);
        }
    }

    @Override
    public String[] getViolationPhotos(String violationPhotosReferenceId) throws ScraperException {
        try {
            HttpPost httpPost = new HttpPost(serviceUrl + "/violationPhotos");
            httpPost.setEntity(
                    EntityBuilder.create()
                            .setContentEncoding("UTF-8")
                            .setContentType(ContentType.APPLICATION_JSON)
                            .setText(
                                    "{" +
                                            "\"violation_photos_reference_id\": \"" + violationPhotosReferenceId + "\"" +
                                            "}")
                            .build()
            );
            CloseableHttpResponse response = httpClient.execute(httpPost);
            try {
                if (response.getStatusLine().getStatusCode() != 200) {
                    if (response.getStatusLine().getStatusCode() == 401) {
                        auth();
                        return getViolationPhotos(violationPhotosReferenceId);
                    }
                    String errorMessage = EntityUtils.toString(response.getEntity(), "UTF-8");
                    throw new ScraperException(errorMessage);
                }
                String[] violationPhotos = objectMapper.readValue(
                        response.getEntity().getContent(),
                        String[].class
                );
                EntityUtils.consumeQuietly(response.getEntity());
                return violationPhotos;
            } finally {
                response.close();
            }
        } catch (IOException e) {
            throw new ScraperException(e.getMessage(), e);
        }
    }

    public void auth() throws ScraperException {
        try {
            HttpPost httpPost = new HttpPost(serviceUrl + "/auth");
            httpPost.setEntity(
                    EntityBuilder.create()
                            .setContentEncoding("UTF-8")
                            .setContentType(ContentType.APPLICATION_JSON)
                            .setText(
                                    "{" +
                                            "\"login\": \"" + login + "\"," +
                                            "\"password\": \"" + password + "\"" +
                                            "}")
                            .build()
            );
            CloseableHttpResponse response = httpClient.execute(httpPost);
            try {
                if (response.getStatusLine().getStatusCode() != 200) {
                    String errorMessage = EntityUtils.toString(response.getEntity(), "UTF-8");
                    throw new ScraperException(errorMessage);
                }
                EntityUtils.consumeQuietly(response.getEntity());
            } finally {
                response.close();
            }
        } catch (IOException e) {
            throw new ScraperException(e.getMessage(), e);
        }
    }

    private boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

}
